﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pryFerreyrosSite.Controllers
{
    public class AccesoController : Controller
    {
        // GET: Acceso
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string User, string Pass)
        {
            try
            {
                using (Models.ferreyrosbdEntities1 db = new Models.ferreyrosbdEntities1())
                {
                    Console.WriteLine("El indicador es : " + User + Pass);
                    var oUser = (from d in db.t_usuario
                                 where d.nombre_usuario == User.Trim() && d.contrasena == Pass.Trim()
                                 select d).FirstOrDefault();
                    Console.WriteLine(oUser);
                    if (oUser == null)
                    {
                        ViewBag.Error = "Usuario o contraseña invalida";
                        return View();
                    }

                    Session["User"] = oUser;

                }

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }

        }
    }
}