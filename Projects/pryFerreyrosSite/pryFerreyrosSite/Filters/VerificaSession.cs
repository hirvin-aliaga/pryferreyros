﻿using pryFerreyrosSite.Controllers;
using pryFerreyrosSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
 
namespace pryFerreyrosSite.Filters
{
    public class VerificaSession : ActionFilterAttribute
    {
        private t_usuario oUsuario;
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                base.OnActionExecuting(filterContext);

                oUsuario = (t_usuario)HttpContext.Current.Session["User"];
                if (oUsuario == null)
                {
                   
                        if (filterContext.Controller is AccesoController == false)
                        {
                            filterContext.HttpContext.Response.Redirect("/Acceso/Login");
                        }
                   
                    

                }

            }
            catch (Exception)
            {
                filterContext.Result = new RedirectResult("~/Acceso/Login");
            }

        }
    }
}