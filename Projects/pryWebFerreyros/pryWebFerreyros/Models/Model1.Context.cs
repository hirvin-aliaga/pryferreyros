﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace pryWebFerreyros.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ferreyrosbdEntitiesFerryros : DbContext
    {
        public ferreyrosbdEntitiesFerryros()
            : base("name=ferreyrosbdEntitiesFerryros")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<t_perfil> t_perfil { get; set; }
        public virtual DbSet<t_seccion> t_seccion { get; set; }
        public virtual DbSet<t_url> t_url { get; set; }
        public virtual DbSet<t_usuario> t_usuario { get; set; }
    }
}
