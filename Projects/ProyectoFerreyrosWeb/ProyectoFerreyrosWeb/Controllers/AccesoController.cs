﻿using ProyectoFerreyrosWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoFerreyrosWeb.Controllers
{
    public class AccesoController : Controller
    {
        // GET: Acceso
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string User, string Pass)
        {
            try
            {
                t_usuario oUsuario = new t_usuario();
                //List<t_perfil> lista_perfil = new List<t_perfil>;
                using (Models.ferreyrosbdEntities db = new Models.ferreyrosbdEntities())
                {
                    var oUser = (from d in db.t_usuario
                                 where d.usuario == User.Trim() && d.contrasena == Pass.Trim()
                                 select d).FirstOrDefault();
                    if (oUser == null)
                    {
                        ViewBag.Error = "Usuario o contraseña invalida";
                        return View();
                    }

                    var oLista_perfil = (from p in db.t_perfil
                                 where p.id_perfil == oUser.perfil.ToString()
                                 select p).FirstOrDefault();

                    //ViewBag.Usuario = oUser;
                    ViewBag.IdPefil = oLista_perfil.id_perfil;
                    ViewBag.Pefil = oLista_perfil.nombre_perfil;
      

                    //oUsuario = new t_usuario();
                    oUsuario.nombre_usuario = oUser.nombre_usuario;
                    oUsuario.apellido_usuario = oUser.apellido_usuario;
                    oUsuario.perfil = oUser.perfil;


                    Session["User"] = oUser;

                }
                

                return RedirectToAction("Index", "Home", oUsuario);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }

        }
    }
}