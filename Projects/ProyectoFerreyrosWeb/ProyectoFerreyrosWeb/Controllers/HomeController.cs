﻿using ProyectoFerreyrosWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoFerreyrosWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(t_usuario oUsuario)
        {

            ViewBag.IdPerfil = oUsuario.perfil;


            try
            {
                //List<t_perfil> lista_perfil = new List<t_perfil>;
                using (Models.ferreyrosbdEntities db = new Models.ferreyrosbdEntities())
                {
                    var oUser = from x in db.t_perfil
                                 join a in db.t_intermedia_perfil_seccion on x.id_perfil equals a.fk_id_perfil
                                 join b in db.t_seccion on a.fk_id_seccion equals b.id_seccion
                                 join c in db.t_url on b.id_seccion equals c.t_seccion
                                 //where == User.Trim() && d.contrasena == Pass.Trim()
                                 select x;
                    if (oUser == null)
                    {
                        ViewBag.Error = "Usuario o contraseña invalida";
                        return View();
                    }

                    var oLista_perfil = (from p in db.t_perfil
                                         where p.id_perfil == oUser.perfil.ToString()
                                         select p).FirstOrDefault();

                    //ViewBag.Usuario = oUser;
                    ViewBag.IdPefil = oLista_perfil.id_perfil;
                    ViewBag.Pefil = oLista_perfil.nombre_perfil;


                    //oUsuario = new t_usuario();
                    oUsuario.nombre_usuario = oUser.nombre_usuario;
                    oUsuario.apellido_usuario = oUser.apellido_usuario;
                    oUsuario.perfil = oUser.perfil;


                    Session["User"] = oUser;

                }


                return RedirectToAction("Index", "Home", oUsuario);
            }
            catch (Exception ex)
            {

            }

              
              

                return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}