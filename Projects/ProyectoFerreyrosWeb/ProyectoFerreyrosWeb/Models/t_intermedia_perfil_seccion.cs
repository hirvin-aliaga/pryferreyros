//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProyectoFerreyrosWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class t_intermedia_perfil_seccion
    {
        public string fk_id_perfil { get; set; }
        public string fk_id_seccion { get; set; }
        public long id_perfil_seccion { get; set; }
    
        public virtual t_perfil t_perfil { get; set; }
        public virtual t_seccion t_seccion { get; set; }
    }
}
